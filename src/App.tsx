import * as React from 'react';
import './App.css';

import logo from './logo.svg';

import * as momentTZ from 'moment-timezone';
import {SelectBase as Select} from 'react-select';
import { getTimezonesSelectOptions, getTimeZones, ITimeZoneSelectOption } from './helpers/tzHelper';

// todo - Watch this https://react-select.com/advanced

const timezoneClick = () => {
  console.log('Madeness');
  const timeZones = momentTZ.tz.names();
  console.log(timeZones.length);
  timeZones.forEach((zone: string) => {
    const offset = momentTZ.tz(zone).format("Z");

    const timeOffset = offset.split(':')[0];
    const numberTimeOffset = parseInt(timeOffset, 10);

    if (typeof numberTimeOffset !== 'number') {
      console.warn(`${offset} can not be parsed correctly`);
    }

    console.log(`Zone: ${zone} Offset: ${offset} Number: ${numberTimeOffset}`);
  });
}

export interface ISelectState {
  isOpen: boolean;
  option: ITimeZoneSelectOption;
  inputString: string;
}

const initialState: ISelectState = {
  isOpen: false,
  option: {
    label: 'default',
    value: 'default'
  },
  inputString: ''
}

class App extends React.Component<{}, ISelectState> {
  
  private options: ITimeZoneSelectOption[]
  
  constructor(props: any) {
    super(props);
    this.state = initialState;

    this.options = getTimezonesSelectOptions(getTimeZones());
  }

  public clearSearch = () => {
    this.setState({
      inputString: ''
    })
  }

  public openSelect = () => {
    console.log('openSelect');
    this.setState({
      isOpen: true
    });
};

  public closeSelect = () => {
      console.log('closeSelect');
      this.setState({
        isOpen: false
      });
  };

  public setOption = (option: ITimeZoneSelectOption) => {
    this.setState({
      option
    });
    this.clearSearch();
    this.closeSelect();
    // console.log(option);
  }

  public inputChanged = (inputString: string) => {
    console.log(`Input changed`);
    this.setState({
      inputString
    });    
  }
  
  public render() {
    let filteredOptions = [];
    if (this.state.inputString) {
      filteredOptions = this.options.filter((option) => {
        return option.value.indexOf(this.state.inputString) > -1;
      });
    } else {
      filteredOptions = this.options;
    }
    
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Timezones Madeness</h1>
        </header>
        <button 
          className="timezone__button"
          onClick={timezoneClick}
        >
        TimezoneMadeness
        </button>
        <Select
          className={'tzSelect'}
          options={filteredOptions}
          value={this.state.option}
          menuIsOpen={this.state.isOpen}
          onChange={this.setOption}
          onMenuOpen={this.openSelect}
          onMenuClose={this.closeSelect}
          isSearchable={true}
          inputValue={this.state.inputString}
          onInputChange={this.inputChanged}
          classNamePrefix={'timezone-select'}
        />      
      </div>
    );
  }
}

export default App;
