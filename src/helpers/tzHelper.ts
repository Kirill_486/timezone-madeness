import * as momentTZ from 'moment-timezone';

export type ITimeZones = ITimeZoneDetails[];

export interface ITimeZoneDetails {
    name: string;
    offset: string;
    offsetValue: number;
}

export interface ITimeZoneSelectOption {
    value: string;
    label: string;
}

export const breakOffsetToNumber =
(
    offset: string
): number => {
    const timeOffset = offset.split(':')[0];
    const numberTimeOffset = parseInt(timeOffset, 10);
    return numberTimeOffset;
}

export const constructTimeZone =
(
    name: string,
    offset: string
) => {
    const offsetValue = breakOffsetToNumber(offset);
    const temeZoneDetails: ITimeZoneDetails = {
        name,
        offset,
        offsetValue
    }

    return temeZoneDetails;
}

export const getTimeZones =
(): ITimeZones => {
    const timeZoneNames = momentTZ.tz.names();
    const offsets = timeZoneNames.map((zone) => {
        const offset = momentTZ.tz(zone).format('Z z');
        return offset;
    });

    const timeZones = [] as ITimeZones;

    timeZoneNames.forEach((item, index) => {
        const timeZone = constructTimeZone(item, offsets[index]);
        timeZones.push(timeZone);
    });

    return timeZones;
}

export const getTimezonesSelectOptions =
(
    timeZones: ITimeZones
): ITimeZoneSelectOption[] => {

    const selectOptions = timeZones.map((item: ITimeZoneDetails): ITimeZoneSelectOption => {
        return {
            label: `${item.name} ${item.offset}`,
            value: item.name
        }
    });

    return selectOptions;
}
